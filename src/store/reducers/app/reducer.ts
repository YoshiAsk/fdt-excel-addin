import produce from 'immer';
import { appConstants } from '../../actions/app/actionTypes';
import { getTranslation } from '../../../helpers/translation';
import _ from 'lodash';

const initialState = { 
  lang: localStorage.getItem('lang') || 'en-us',
  translate: getTranslation('en-us'),
  error: null,
  prevWorksheet: null,
  currentWorksheet: null,
  currentShape: null,
  activatedSheets: [],
  contacts: {
    existsSheet: false,
    items: []
  },
  proposals: {
    existsSheet: false,
    items: []
  },
  appConfig: {
    appName: '',
    appVersion: '',
    appDate: '',
    appPassword: '',
    contactsUrl: '',
    uploadFile: '',
    logosWorksheet: 'Logos',
    logosWorksheetws141: '1.41-Proposal Letter',
    logosTable: 'Table.Logos',
    logosNX: 'NX',
    logoCurrentLogo: 'CurrentLogo',
    logoTableColumn: 'I',
    logoTableHeaderRow: '8',
    logoWorksheets: [],
    contact: {
      rangeName: 'ContactName',
      rangeEmail: 'ContactEmail',
      rangePhone: 'CompanyPhone',
      rangeContacts: 'Query1',
      worksheet: '1.42-Clarif. & Excl.'
    },
    proposal: {
      rangeProposal: 'Proposal',
      tableProposals: 'Table.Scope',
      worksheet: 'Info'
    },
    autosize: {
      rangeAutoSize: ''
    }       
  },
  longOperation: {
    isFetched: false,
    fetchMessage: ''
  },
  printSheets: '1.41-Proposal Letter,1.42-Clarif. & Excl.'
};

export function appReducer(state = initialState, action) {
  switch (action.type) {
    case appConstants.APP_INITIALIZE_ADDIN:
      return produce(state, draft => {
        draft.translate = getTranslation(action.payload.displayLanguage);
        draft.appConfig = action.payload.appConfig;
      });
    case appConstants.APP_INITIALIZE_CONTACTS:
      return produce(state, draft => {
        draft.contacts = action.payload.contacts;
      });   
    case appConstants.APP_INITIALIZE_PROPOSALS:
      return produce(state, draft => {
        draft.proposals = action.payload.proposals;
      });           
    case appConstants.APP_DISPLAY_ERROR:
      return {
        ...state,
        error: action.payload.error
      };         
    case appConstants.APP_CLEAR_ERRORS:
      return {
        ...state,
        error: null
      }; 
    case appConstants.APP_START_FETCH:
      return {
        ...state,
        longOperation: {
          isFetched: true,
          fetchMessage: action.payload.fetchMessage
        }
      };
    case appConstants.APP_STOP_FETCH:
      return {
        ...state,
        longOperation: {
          isFetched: false
        }
      };
    case appConstants.APP_ACTIVATE_WORKSHEET:
      return produce(state, draft => {
        if (state.currentWorksheet != action.payload.worksheet) {
          draft.prevWorksheet = state.currentWorksheet;
          draft.currentWorksheet = action.payload.worksheet;
          draft.activatedSheets = state.activatedSheets;
          const index = _.findIndex(draft.activatedSheets, item => { return item === action.payload.worksheet});
          if (index < 0)
            draft.activatedSheets.push(action.payload.worksheet);
        }
      });
    case appConstants.APP_ACTIVATE_SHAPE:
      return produce(state, draft => {
        draft.currentShape = action.payload.shape;
      });
    case appConstants.APP_DEACTIVATE_SHAPE:
      return produce(state, draft => {
        draft.currentShape = null;
      });                         
    default:
      return state;
  }
}
