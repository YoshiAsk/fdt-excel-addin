import { combineReducers } from 'redux';
import { appReducer } from './app/reducer';
import { excelOfficeReducer } from './office/excel/reducer';

const rootReducer = combineReducers({
  appReducer,
  excelOfficeReducer,
});

export default rootReducer;
