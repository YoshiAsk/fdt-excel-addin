/**
 * Main application actions
 */
import { forEach, find, isNull, isUndefined, concat } from 'lodash';
import { store } from '../../../store';
import { appConstants } from './actionTypes';
import { excelApi } from '../../../services/excelApi';
import { appApi } from '../../../services/appApi';
import _ from 'lodash';
import { Proposals, ProposalsItem } from '../../../models/proposals.model';

/**
 * Initializes the addin
 * @param inputLocation 
 */
function initializeAddin(inputLocation: Location) {

    return async dispatch => {

        async function onActivatedWorksheetHandler(sheet: string) {
            console.log('onActivatedWorksheetHandler called');
            console.log('current sheet :', sheet);
            const state = store.getState();
            const index = _.findIndex(state.appReducer.activatedSheets, item => { return item === sheet});
            if (index < 0) {
                const result: boolean = await excelApi.justifyLeft(sheet);
            }
            /*if (sheet == state.appReducer.appConfig.logosWorksheetws141) {
                await excelApi.setProposalHeight(state.appReducer.appConfig);
            }*/
            dispatch({ type: 'app.APP_ACTIVATE_WORKSHEET', payload: { worksheet: sheet } });
        }

        async function onChangedCellContactNameHandler() {
            console.log('onChangedCellContactNameHandler called');
            const state = store.getState();
            await excelApi.setContactFormula(state.appReducer.appConfig);
        }

        async function onChangedCellProposalHandler() {
            console.log('onChangedCellProposalHandler called');
            const state = store.getState();
            //console.log('state.appReducer.proposals :', state.appReducer.proposals);
            await excelApi.setProposalHeight(state.appReducer.appConfig, state.appReducer.proposals);
        }

        async function onChangeCellAutoSizeHandler(sheet: any, address: string, relatedColumn: string) {
            console.log('onChangeCellAutoSizeHandler called');
            const state = store.getState();
            await excelApi.setAutoSizeHeight(state.appReducer.appConfig, address, relatedColumn);
        }

        async function onActivatedShapeHandler(shape) {
            console.log('onActivatedShapeHandler called');
            dispatch({ type: appConstants.APP_ACTIVATE_SHAPE, payload: { shape: shape } });
        }

        async function onDeactivatedShapeHandler(shape) {
            dispatch({ type: appConstants.APP_DEACTIVATE_SHAPE, payload: { } });
        }

        console.log('initializeAddin action called');
        const state = store.getState();

        const config = await appApi.getConfig();
        
        dispatch({ type: appConstants.APP_INITIALIZE_ADDIN, payload: { appConfig: config } }); 

        const result: boolean = await excelApi.initializeWorkbook(config, 
            {
                onActivatedWorksheetHandler: onActivatedWorksheetHandler, 
                onChangedCellContactNameHandler : onChangedCellContactNameHandler,
                onActivatedShapeHandler: onActivatedShapeHandler, 
                onDeactivatedShapeHandler: onDeactivatedShapeHandler, 
                onChangedCellProposalHandler: onChangedCellProposalHandler, 
                onChangeCellAutoSizeHandler: onChangeCellAutoSizeHandler
            });

        const proposals: Proposals = await excelApi.getProposals(state.appReducer.appConfig);
        if (proposals !== null && proposals !== undefined) {
            dispatch({ type:'app.APP_INITIALIZE_PROPOSALS', payload: { proposals: proposals } });
        }
    };
}


export const appActions = {
    initializeAddin
};
