/**
 * Office helper actions
 */

import { isNull, isUndefined } from 'lodash';
import { store } from '../../../../store';
import { excelApi } from '../../../../services/excelApi';
import { Logos, LogosItem } from '../../../../models/logos.model';
import { Contacts, ContactsItem } from '../../../../models/contacts.model';
import { Proposals, ProposalsItem } from '../../../../models/proposals.model';
import { odataStubService } from '../../../../services/odataStubService';

function openWorksheet(sheetName: string) {
    return async dispatch => {
        const result: boolean = await excelApi.activateWorksheetByName(sheetName);
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_worksheet') } });
        }
    };
}

function openNextWorksheet() {
    return async dispatch => {
        const result: boolean = await excelApi.activateNextWorksheet();
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_nextworksheet') } });
        }
    };
}

function openLastWorksheet() {
    return async dispatch => {
        const state = store.getState();
        if (state.appReducer.prevWorksheet != null && state.appReducer.prevWorksheet != undefined && state.appReducer.prevWorksheet !== '') {
            console.log('state.appReducer.prevWorksheet=', state.appReducer.prevWorksheet);
            const result: boolean = await excelApi.activateWorksheetByName(state.appReducer.prevWorksheet);
            dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
            if (!result) {
                const state = store.getState();
                dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_worksheet') } });
            }
        }
        else
            dispatch({ type:'app.APP_STOP_FETCH', payload: { } });        
    };
}

function openBackWorksheet() {
    return async dispatch => {
        const result: boolean = await excelApi.activatePreviousWorksheet();
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_backworksheet') } });
        }
    };
}

function setCalculationMode(mode: Excel.CalculationMode) {
    return async dispatch => {
        if (Office.context.requirements.isSetSupported('ExcelApi', '1.8')) {
            const result: boolean = await excelApi.setCalculationMode(mode);
            dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
            if (!result) {
                const state = store.getState();
                dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_calculationmode') } });
            }
        }
        else
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: 'The Excel does not support set calculation mode' } });
    };
}

function setLogo(logo: LogosItem) {
    return async dispatch => {
        const state = store.getState();
        const result: boolean = await excelApi.setLogo(state.appReducer.appConfig, logo);
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_setlogo') } });
        }
    };
}

function getContacts() {
    return async dispatch => {
        const state = store.getState();
        const contacts: Contacts = await excelApi.getContacts(state.appReducer.appConfig);
        if (contacts !== null && contacts !== undefined) {
            dispatch({ type:'app.APP_INITIALIZE_CONTACTS', payload: { contacts: contacts } });
        } else {
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_getcontacts') } });
        }
    };
}

function getProposals() {
    return async dispatch => {
        const state = store.getState();
        const proposals: Proposals = await excelApi.getProposals(state.appReducer.appConfig);
        if (proposals !== null && proposals !== undefined) {
            dispatch({ type:'app.APP_INITIALIZE_PROPOSALS', payload: { contacts: proposals } });
        } else {
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_getproposals') } });
        }
    };
}

function setContact(contact: ContactsItem) {
    return async dispatch => {
        const state = store.getState();
        const result: boolean = await excelApi.setContact(state.appReducer.appConfig, contact);
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_setcontact') } });
        }
    };
}

function setProposal(proposal: ProposalsItem) {
    return async dispatch => {
        const state = store.getState();
        const result: boolean = await excelApi.setProposal(state.appReducer.appConfig, proposal);
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        if (!result) {
            const state = store.getState();
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_setproposal') } });
        }
    };
}

function convertToPdf() {
    return async dispatch => {
        const state = store.getState();
        const content = await excelApi.getWorkbookContent(state.appReducer.appConfig);
        const blob = new Blob([new Uint8Array(content)], {type: 'application/pdf'});
        const result = await odataStubService.uploadFile(state.appReducer.appConfig, blob, 'export.pdf');

        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });

        if (result == null) {
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_pdfconvert') } });
        }
        else
            window.open(state.appReducer.appConfig.uploadFile + '/' + result.file);

    };
}

function print() {
    return async dispatch => {
        const state = store.getState();
        const content = await excelApi.print(state.appReducer.appConfig);
        const blob = new Blob([new Uint8Array(content)], {type: 'application/pdf'});
        const result = await odataStubService.uploadFile(state.appReducer.appConfig, blob, 'export.pdf');

        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });

        if (result == null) {
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_pdfconvert') } });
        }
        else
            window.open(state.appReducer.appConfig.uploadFile + '/' + result.file);

    };
}

export const excelOfficeActions = {
    openWorksheet,
    openNextWorksheet,
    openBackWorksheet,
    openLastWorksheet,
    setCalculationMode,
    setLogo,
    getContacts,
    setContact,
    convertToPdf,
    getProposals,
    setProposal,
    print
};

