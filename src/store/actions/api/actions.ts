/**
 * Office helper actions
 */
import { isNull, isUndefined } from 'lodash';
import { odataStubService } from '../../../services/odataStubService';
import { store } from '../../../store';
import { Contacts, ContactsItem } from '../../../models/contacts.model';

function getContacts() {

    return async dispatch => {

        console.log(`getContacts API`);

        const state = store.getState();
        const result: Contacts = await odataStubService.getContacts(state.appReducer.appConfig);

        console.log(`getContacts API : ${JSON.stringify(result)}`);

        const contacts: Contacts = await odataStubService.getContacts(state.appReducer.appConfig);
        if (contacts !== null && contacts !== undefined) {
            dispatch({ type:'app.APP_INITIALIZE_CONTACTS', payload: { contacts: contacts } });
        } else {
            dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: state.appReducer.translate('error_getcontacts') } });
        }
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    };
}

export const apiActions = {
    getContacts
};
