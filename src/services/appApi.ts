import axios from 'axios';
import _ from 'lodash';
import { store } from '../store';

 /**
  * Load config
  */
const getConfig = async () => {
    console.log('getConfig is called');
    const state = store.getState();
    if (state.appReducer.appConfig.appName.length <= 0) {
        return (await axios.get('assets/config.json')).data;
    }
    return state.appReducer.appConfig;
}

export const appApi = {
    getConfig
}