import _ from 'lodash';
import { Logos, LogosItem} from '../models/logos.model';
import { Contacts, ContactsItem} from '../models/contacts.model';
import { Proposals, ProposalsItem} from '../models/proposals.model';

/**
 * Contains implementation of Excel Api wrapper
 */

//////////////////////////////////////////////////////////////////////////////////////
// Private methods

const toColumnName = (num: any): string => {
    let ret = '';
    let a = 1;
    let b = 26;
    for (ret = '', a = 1, b = 26; (num -= a) >= 0; a = b, b *= 26) {
      // tslint:disable-next-line:radix
      const value = ((num % b) / a) + 65;
      ret = String.fromCharCode(value) + ret;
    }
    return ret;
}

const getWorksheetByName = (sheets, sheetName: string): Excel.Worksheet => {
    return sheets.items.find(item => item.name.toLowerCase() === sheetName.toLowerCase());
}

const getShapeByName = (shapes, name: string): any => {
    return shapes.items.find(item => item.name.toLowerCase() === name.toLowerCase());
}

/**
 * Returns the index of last row in the sheet
 * @param context 
 */
async function getSheetLastRowIndex(context: any, sheet: any) {
    try {
        const uRowsIndex = sheet.getUsedRange().getLastCell().load(['rowIndex']);
        await context.sync();
        console.log(`getSheetLastRowIndex: Last column index: ${uRowsIndex.rowIndex + 1}`);
        return uRowsIndex.rowIndex + 1;
    } catch(err) {
        console.log('Error getSheetLastRowIndex: ', err);
    }
    return 1;
}

/**
 * Returns the index of last column in the sheet
 * @param context 
 */
async function getSheetLastColumnIndex(context: any, sheet: any) {
    try {
        const uColumnsIndex = sheet.getUsedRange().getLastCell().load(['columnIndex']);
        await context.sync();
        console.log(`getSheetLastColumnIndex: Last column index: ${uColumnsIndex.columnIndex + 1}`);
        return uColumnsIndex.columnIndex + 1;
    } catch(err) {
        console.log('Error getSheetLastColumnIndex: ', err);
    }
    return 26;
}


/**
 * 
 * Returns the range for shape
 */
async function getShapeRange(context: any, sheet: any, left: number, top: number) {
    console.log('getShapeRange called');
    try {
        let column = -1;
        let row = -1;

        const rowIndex = await getSheetLastRowIndex(context, sheet);
        const columnIndex = await getSheetLastColumnIndex(context, sheet);

        for (let i = 1; i <= _.toNumber(columnIndex); i++) {
            //console.log(`${toColumnName(i)}1`);
            const rowRange = sheet.getRange(`${toColumnName(i)}1`);
            rowRange.load("left");
            await context.sync();
            if (rowRange.left === left || i == columnIndex) {
                column = i;
                break;
            } 
            else if (rowRange.left > left + 1) {
                column = i - 1;
                break;
            }
        }

        if (column > 0) {
            for (let i = 1; i <= _.toNumber(rowIndex); i++) {
                //console.log(`A${i}`);
                const columnRange = sheet.getRange(`A${i}`);
                columnRange.load("top");
                await context.sync();
                if (columnRange.top === top || i == rowIndex) {
                    row = i;
                    break;
                }
                else if (columnRange.top > top + 1) {
                    row = i - 1;
                    break;
                }
            }
        }

        if (column > 0 && row > 0) {
            return `${toColumnName(column)}${(row)}`
        }
    } catch(err) {
        console.log('Error getShapeRange: ', err);
    }
    return null;
}

/**
 * Protects the specified worksheet
 * @param context 
 * @param sheet 
 */
async function protectSheet(context: any, sheet: any, password: string) {
    try {
        console.log(`Protects the worksheet '${sheet.name}'`);
        sheet.load('protection/protected');
        await context.sync();
        if (!sheet.protection.protected) {
            console.log('sets all cells unlocked');
            const entireRange = sheet.getRange();
            entireRange.format.protection.locked = false;
            await context.sync();
            sheet.protection.protect({
                allowEditObjects: true,
                allowEditScenarios: true,
            }, password);
            await context.sync();
            console.log(`the sheet is protected`);
        } else {
            console.log(`The worksheet '${sheet.name}' already protected`);
        }
    } catch(err) {
        console.log('Error: ', err);
    }
}

/**
 * Unprotects the specified worksheet
 * @param context 
 * @param sheet 
 */
async function unprotectSheet(context: any, sheet: any, password: string) {
    try {
        sheet.load('protection/protected');
        await context.sync();
        if (sheet.protection.protected) {
            console.log(`unprotect the worksheet`);
            sheet.protection.unprotect(password);
            await context.sync();
            console.log(`the sheet is unprotected`);
        }
    } catch(err) {
        console.log('Error: ', err);
    }
}

/**
 * Is sheet protected
 * @param context 
 * @param sheet 
 */
async function isProtectSheet(context: any, sheet: any) {
    try {
        sheet.load('protection/protected');
        await context.sync();
        return sheet.protection.protected;
    } catch(err) {
        console.log('Error: ', err);
    }

    return false;
}

async function getDataSlice(state): Promise<any> {
    console.log(`getDataSlice called with counter: ${state.counter}`);
    return new Promise((resolve, reject) => {
        state.file.getSliceAsync(state.counter, result => {
            console.log(`getSliceAsync returned the result.status: ${result.status}`);
            if (result.status == Office.AsyncResultStatus.Succeeded) {
                console.log(`Reads slice ${state.counter}`);
                state.counter++;
                if (state.counter < state.sliceCount) {
                    console.log(`Received ${result.value.data.length} size. total size is ${state.documentData.length}`);
                    state.documentData = state.documentData.concat(result.value.data);

                    const percentCompleted = state.documentData.length / state.file.size;
                    console.log(`stateResult.documentData.length = ${state.file.size}; progressEvent.loaded = 0; percentCompleted = ${percentCompleted}`);
                    

                    getDataSlice(state)
                    .then(stateResult => {
                        resolve(stateResult);
                    });
                } else {
                    state.documentData = state.documentData.concat(result.value.data);
                    resolve(state);
                }
            }
            else {
                resolve(null);
            }
        });
    });
}

function closeFile(state): Promise<any> {
    console.log(`closeFile called with counter: ${state.counter}`);
    return new Promise((resolve, reject) => {
        state.file.closeAsync(result => {
            if (result.status == "succeeded") {
                resolve(true);
            }
            else {
                resolve(false);
            }
        });
    });
}

/**
 * Get range address
 * @param context 
 * @param appConfig 
 * @param sheet 
 */
async function GetRangeAddress(context: any, rangeName: string, sheet: any) {
    try {
        const range = sheet.getRange(rangeName);
        range.load("address");
        await context.sync();
        let address: string = range.address;
        if (address.indexOf("!") > 0)
            address = address.substring(address.indexOf("!") + 1);
        console.log("address: " + address);
        return address;
    } catch(err) {
        console.log('Error: ', err);
    }

    return '';
}

const getAutoSizeCells = (autosizes) => {
    const ranges = [];

    _.forEach(autosizes, range => {
        const cells = range.split(':');
        const column = cells[0].substring(0,1);
        const start: number = _.toNumber(cells[0].substring(1));
        const end: number = _.toNumber(cells[1].substring(1));
        const relatedColumn: string = (cells.length > 2) ? cells[2] : '';
        ranges.push({ column: column, start: start, end: end, relatedColumn: relatedColumn });
    })
    return ranges;
}

//////////////////////////////////////////////////////////////////////////////////////
// Public export methods

 /**
  * Activates the last worksheets of workbook
  */
const activateLastWorksheet = async (): Promise<boolean> => {
    console.log('activateLastWorksheet is called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(context => {
                const sheet = context.workbook.worksheets.getLast();
                sheet.activate();
                return context.sync()
                .then(() => {
                    resolve(true);
                });
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Activates next worksheet
 */
const activateNextWorksheet = async (): Promise<boolean> => {
    console.log('activateNextWorksheet is called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(context => {
                const currentSheet = context.workbook.worksheets.getActiveWorksheet();
                const sheet = currentSheet.getNext();
                sheet.activate();
                return context.sync()
                .then(() => {
                    resolve(true);
                });
            })
            .catch(error => {
                console.log('Error: ', error);
                if (error.code === 'ItemNotFound') //for last worksheet
                    resolve(true);
                else
                    resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Activates previous worksheet
 */
const activatePreviousWorksheet = async (): Promise<boolean> => {
    console.log('activatePreviousWorksheet is called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(context => {
                const currentSheet = context.workbook.worksheets.getActiveWorksheet();
                const sheet = currentSheet.getPrevious();
                sheet.activate();
                return context.sync()
                .then(() => {
                    resolve(true);
                });
            })
            .catch(error => {
                console.log('Error: ', error);
                if (error.code === 'ItemNotFound') //for first worksheet
                    resolve(true);
                else
                    resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * 
 * Activates the worksheet by name
 * @param sheetName
 */
export async function activateWorksheetByName(sheetName: string): Promise<any> {
    console.log('activateWorksheetByName called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {
                var sheet = context.workbook.worksheets.getItem(sheetName);
                sheet.activate();
                await context.sync();
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * 
 * Returns the active sheet name
 */
export async function getActiveWorkseet(): Promise<any> {
    console.log('getActiveWorkseet called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {
                var sheet = context.workbook.worksheets.getActiveWorksheet();
                sheet.load("name");
                await context.sync();
                resolve(sheet.name);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Initializes the workbook
 * Use only from panel
 * @param events: onActivatedWorksheetHandler, onChangedCellContactNameHandler,
    onActivatedShapeHandler, onDeactivatedShapeHandler, onChangedCellProposalHandler,
    onChangeCellAutoSizeHandler
 */
export async function initializeWorkbook(appConfig: any, events): Promise<boolean> {
    console.log('initializeWorkbook called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                // Set current worksheet
                const sheet = await getActiveWorkseet();
                if (events.onActivatedWorksheetHandler !== null && events.onActivatedWorksheetHandler !== undefined)
                    events.onActivatedWorksheetHandler(sheet);

                // onActivated event
                const workbookSheets = context.workbook.worksheets;
                workbookSheets.onActivated.add(async (args : Excel.WorksheetActivatedEventArgs) => {
                    console.log('onActivated  event for workbooksheet');

                    const sheet = await getActiveWorkseet();
                    console.log('current sheet :', sheet);
                    if (events.onActivatedWorksheetHandler !== null && events.onActivatedWorksheetHandler !== undefined)
                        events.onActivatedWorksheetHandler(sheet);
                });

                // onChanged event
                workbookSheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(workbookSheets, appConfig.logosWorksheetws141);
                if (sheetProposal !== null && sheetProposal !== undefined) {

                    const contactAddress: string = await GetRangeAddress(context, appConfig.contact.rangeName, sheetProposal);
                    const proposalAddress: string = await GetRangeAddress(context, appConfig.proposal.rangeProposal, sheetProposal);
                    const autosizeAddresses = getAutoSizeCells(appConfig.autosize.rangeAutoSize.split(','));

                    sheetProposal.onChanged.add(async event => {
                        console.log('onChanged event');
                        console.log("contactAddress: " + contactAddress);
                        console.log("proposalAddress: " + proposalAddress);
                        console.log("Address of event: " + event.address);
                        if (event.address === contactAddress) {
                            if (events.onChangedCellContactNameHandler !== null && events.onChangedCellContactNameHandler !== undefined)
                                events.onChangedCellContactNameHandler(sheet);
                        }
                        else if (event.address === proposalAddress) {
                            if (events.onChangedCellProposalHandler !== null && events.onChangedCellProposalHandler !== undefined)
                                events.onChangedCellProposalHandler(sheet);
                        }
                        else {
                            try {
                                //const changedRange = event.getRangeOrNullObject(context);
                                //const changedRange = sheetProposal.getRange(event.address);
                                _.forEach (autosizeAddresses, address => {
                                    //const changedRange1 = sheetProposal.getRange(address);
                                    //const rangeIntersection = changedRange1.getIntersectionOrNullObject(changedRange);
                                    const row = _.toNumber(event.address.substring(1));
                                    if (event.address.substring(0,1) === address.column &&
                                        row >= address.start && row <= address.end) {
                                        console.log("rangeIntersection: " + event.address);
                                        if (events.onChangeCellAutoSizeHandler !== null && events.onChangeCellAutoSizeHandler !== undefined)
                                            events.onChangeCellAutoSizeHandler(sheet, event.address, address.relatedColumn);
                                    };

                                })
                            }
                            catch (err) {}

                        }                  
                    });
                }
            
                var activeSheet = context.workbook.worksheets.getActiveWorksheet();
                activeSheet.load("name");
                await context.sync();
                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetItems = sheets.items;

                /*
                for (const sheet of sheetItems) {
                    console.log('sheet.name = ', sheet.name);

                    // #12 - "Left Justify" all sheets when the task panel opens
                    const rangeA1 = sheet.getRange('A1');
                    console.log('Activate A1');
                    rangeA1.select();
                    await context.sync();
                }
                activeSheet.activate(); 
                await context.sync();*/

                // Add activate/deactivate events for all shapes
                for (const sheet of sheetItems) {
                    console.log('sheet.name = ', sheet.name);

                    sheet.shapes.load('items/name,items/type');
                    await context.sync();
                    const shapeItems = sheet.shapes.items;
                    for (const shape of shapeItems) {
                        shape.onActivated.add(async (args : Excel.ShapeActivatedEventArgs) => {
                            console.log('onActivated event for shape');
                            if (events.onActivatedShapeHandler !== null && events.onActivatedShapeHandler !== undefined)
                                events.onActivatedShapeHandler(shape);
                        });
                        shape.onDeactivated.add(async (args : Excel.ShapeDeactivatedEventArgs) => {
                            console.log('onDeactivated event for shape');
                            if (events.onDeactivatedShapeHandler !== null && events.onDeactivatedShapeHandler !== undefined)
                                events.onDeactivatedShapeHandler(shape);
                        });                       
                    }
                }

                //activeSheet.activate(); 
                await context.sync();
                console.log("A handler has been registered for the OnActivate event.");

                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * 
 * Returns the list of worksheets
 */
export async function getWorksheets(): Promise<any> {
    console.log('getWorkseets called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {
                const names = [];
                const sheets = context.workbook.worksheets;
                sheets.load('items/name,items/visibility');
                await context.sync();

                const items = sheets.items;
                _.forEach(items, item => {
                    //console.log(sheet.visibility);
                    if (item.visibility== Excel.SheetVisibility.visible) {
                        console.log(`The worksheet '${item.name}' added to list.`);
                        names.push(item.name);
                    }
                });
                resolve(names);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}


/**
 * Gets calculation mode
 */
const getCalculationMode = async (): Promise<any> => {
    console.log('activatePreviousWorksheet is called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {
                const app = context.workbook.application;
                app.load("calculationMode");
                await context.sync();
                console.log('app.calculationMode = ', app.calculationMode);
                resolve(app.calculationMode);
            })
            .catch(error => {
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Gets calculation mode
 * @param mode - calculation mode
 */
const setCalculationMode = async (mode: Excel.CalculationMode): Promise<boolean> => {
    console.log('setCalculationMode is called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {
                if (Office.context.requirements.isSetSupported('ExcelApi', '1.8')) {
                    const app = context.workbook.application;
                    app.calculationMode = mode;
                    await context.sync();
                    resolve(true);
                }
                else 
                    resolve(false);
            })
            .catch(error => {
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Gets logos list
 */
export async function getLogos(appConfig: any): Promise<any> {
    console.log('getLogos called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();

                const logos : Logos = {
                    existsSheet: false,
                    items: []
                };

                const logoSheet = getWorksheetByName(sheets, appConfig.logosWorksheet);
                
                if (logoSheet !== null && logoSheet !== undefined) {
                    logos.existsSheet = true;
                    const ranges = logoSheet.getRange(appConfig.logosTable);
                    ranges.load('values');
                    await context.sync();
                    const values = ranges.values;
                    
                    const items = _.map(values, (row) => {
                        //console.log('logosWorksheet = : ', row[1]);
                        return {
                            index: _.toNumber(row[0]),
                            logoName: row[1],
                            acronym: row[2],
                            company: row[3]
                        }
                    });
                    //console.log('set items');
                    logos.items = items;
                }
                resolve(logos);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Sets logo
 * @param LogosItem - logo
 */
export async function setLogo(appConfig: any, logo: LogosItem): Promise<boolean> {
    console.log('setLogo called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                if (!Office.context.requirements.isSetSupported('ExcelApi', '1.8')) {
                    resolve(false);
                    return;
                }

                const sheets = context.workbook.worksheets;
                sheets.load('items/name,items/visibility,items/protection/protected');
                await context.sync();
                const sheetItems = sheets.items;

                // calc needs to be on automatic
                const app = context.workbook.application;
                app.calculationMode = Excel.CalculationMode.automatic;
                await context.sync();

                const wsStartSheet = context.workbook.worksheets.getActiveWorksheet();
                const wsLogoSheet = getWorksheetByName(sheets, appConfig.logosWorksheet);
                console.log('state.appReducer.appConfig.logosWorksheet = ', appConfig.logosWorksheet);
                
                let result = false;
                if (wsLogoSheet !== null && wsLogoSheet !== undefined) {
                    let ranges = wsLogoSheet.getRange(appConfig.logosNX);
                    ranges.values = [[logo.index]];
                    await context.sync();

                    // Check existing logo in Logos sheet
                    const logoName = logo.logoName;
                    wsLogoSheet.shapes.load('items/name,items/left,items/top');
                    await context.sync();
                    let shape = getShapeByName(wsLogoSheet.shapes, logoName);
                    const bLogoXExists: boolean = (shape !== null && shape !== undefined) ? true : false;
                    console.log('bLogoXExists = ', bLogoXExists);
                    if (bLogoXExists) {
                        //console.log('shape.left/top = ', shape.left, shape.top);
                        //const logoRange = await getShapeRange(context, wsLogoSheet, shape.left, shape.top);
                        const logoRange = appConfig.logoTableColumn + (_.toNumber(appConfig.logoTableHeaderRow) + logo.index);
                        console.log('logoRange = ', logoRange);

                        for (const sheet of sheetItems) {
                            console.log('sheet.name = ', sheet.name);
                            sheet.shapes.load('items/name,items/left,items/top');
                            await context.sync();
                            console.log('state.appReducer.appConfig.logoCurrentLogo = ', appConfig.logoCurrentLogo);
                            shape = getShapeByName(sheet.shapes, appConfig.logoCurrentLogo);
                            const bCurrentLogoExists: boolean = (shape !== null && shape !== undefined) ? true : false;
                            console.log('bCurrentLogoExists = ', bCurrentLogoExists);

                            const configSheet = _.find(appConfig.logoWorksheets, ['name', sheet.name]);

                            if (bCurrentLogoExists && configSheet !== null && configSheet !== undefined) {

                                //const currentRange = await getShapeRange(context, sheet, shape.left, shape.top);
                                let currentRange = configSheet.imageRange;
                                console.log('currentRange = ', currentRange);

                                if (logoRange !== null && logoRange !== undefined && currentRange !== null && currentRange !== undefined) {

                                    if (configSheet.protected)
                                        await unprotectSheet(context, sheet, appConfig.appPassword);

                                    //app.suspendScreenUpdatingUntilNextSync();

                                    // delete old image
                                    shape.delete();
                                    await context.sync();

                                    // Copy image
                                    const rangeSource = wsLogoSheet.getRange(logoRange);
                                    const rangeDest = sheet.getRange(currentRange);
                                    rangeDest.copyFrom(rangeSource);
                                    await context.sync();

                                    // Rename to current logo
                                    sheet.shapes.load("items/name,count");
                                    await context.sync();
                                    const result: OfficeExtension.ClientResult<number> = sheet.shapes.getCount();
                                    await context.sync();
                                    const count: number = result.value;
                                    let newShape = sheet.shapes.getItemAt(count - 1);
                                    newShape.name = appConfig.logoCurrentLogo;
                                    await context.sync();

                                    // remove border
                                    const rangeCurrent = sheet.getRange(currentRange);
                                    rangeCurrent.format.load('borders');
                                    await context.sync();
                                    rangeCurrent.format.borders.getItem('EdgeBottom').style = 'None';
                                    rangeCurrent.format.borders.getItem('EdgeLeft').style = 'None';
                                    rangeCurrent.format.borders.getItem('EdgeRight').style = 'None';
                                    rangeCurrent.format.borders.getItem('EdgeTop').style = 'None';
                                    await context.sync();

                                    if (configSheet.protected)
                                        await protectSheet(context, sheet, appConfig.appPassword);
                                }
                            }
                        }
                        result = true;
                    }
                }
                resolve(result);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}


/**
 * Sets contact value
 */
export async function setContact(appConfig: any, contact: ContactsItem): Promise<any> {
    console.log('setContact called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined) {
                    const range = sheetProposal.getRange(appConfig.contact.rangeName);
                    range.values = [[contact.name]];
                    await context.sync();
                    console.log('setContact done');
                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Sets contact formula
 */
export async function setContactFormula(appConfig: any): Promise<any> {
    console.log('setContactFormula called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined) {
                    const rangeEmail = sheetProposal.getRange(appConfig.contact.rangeEmail);
                    rangeEmail.load("address");
                    await context.sync();
                    console.log('rangeEmail.address', rangeEmail.address);
                    rangeEmail.formulas = [["=IFERROR(INDEX(Contacts!Email,MATCH(ContactName,Contacts!Name,0)),\"\")"]];
                    await context.sync();

                    const rangePhone = sheetProposal.getRange(appConfig.contact.rangePhone);
                    rangePhone.load("address");
                    await context.sync();
                    console.log('rangePhone.address', rangePhone.address);
                    rangePhone.formulas = [["=IFERROR(INDEX(Contacts!DirectPhone,MATCH(ContactName,Contacts!Name,0)),\"\")"]];
                    await context.sync();

                    console.log('setContactFormula done');
                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Sets contact name/email/phone
 */
export async function setFullContact(appConfig: any, contact: ContactsItem): Promise<any> {
    console.log('setContactFormula called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined) {
                    const range = sheetProposal.getRange(appConfig.contact.rangeName);
                    range.values = [[contact.name]];
                    await context.sync();

                    const rangeEmail = sheetProposal.getRange(appConfig.contact.rangeEmail);
                    rangeEmail.values = [[contact.email]];
                    await context.sync();

                    const rangePhone = sheetProposal.getRange(appConfig.contact.rangePhone);
                    rangePhone.values = [[contact.phone]];
                    await context.sync();

                    console.log('setFullContact done');
                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}


/**
 * Sets Proposal height
 */
export async function setProposalHeight(appConfig: any, proposals: Proposals): Promise<any> {
    console.log('setProposalHeight called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined) {

                    let range = sheetProposal.getRange(appConfig.proposal.rangeProposal);
                    range.load('values,format,numberFormat');
                    await context.sync();
                    const proposal: string = range.values[0][0];

                    //console.log('proposals ', proposals);
                    const index = _.findIndex(proposals.items, item => { return item.costScope === proposal || item.scopeText === proposal});
                    if (index >= 0) {
                        range.format.rowHeight = proposals.items[index].rowHeight;
                        await context.sync();
                    }

                    /*
                    console.log('range.values', range.values[0][0]);
                    range.format.rowHeight = 200;
                    await context.sync();
                    //sheetProposal.calculate(false);
                    range = sheetProposal.getRange('D28');//.getEntireRow();
                    //range.load('values,format,numberFormat');
                    //await context.sync();
                    range.values = "dhghdgd \njdhjhsdj";
                    range.format.autofitRows();
                    await context.sync();

                    console.log('setProposalHeight done');

                    setProposalHeight
                    */
                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}


/**
 * Sets AutoSize height
 */
export async function setAutoSizeHeight(appConfig: any, address: string, relatedColumn: string): Promise<any> {
    console.log('setAutoSizeHeight called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined && relatedColumn !== '') {

                    const range = sheetProposal.getRange(address);
                    range.load('values,format,numberFormat,wrapText');
                    await context.sync();
                    const value = range.values[0][0];
                    const row = range.getEntireRow();

                    let rangeRelated = sheetProposal.getRange(relatedColumn + address.substring(1));
                    rangeRelated.values = [[value]];
                    await context.sync();
                    rangeRelated.calculate();  
                    
                    row.format.autofitRows();
                    await context.sync();

                    console.log('setAutoSizeHeight done ', address);

                    /*
                    rangeRelated = sheetProposal.getRange(relatedColumn + address.substring(1));
                    rangeRelated.load('values,height');
                    await context.sync();
                    const height = rangeRelated.height;
                    console.log('height ', height);
                    rangeRelated.formulas = "=" + address;*/
                    //await context.sync();

                    //row.load('values,format,numberFormat,wrapText');
                    //await context.sync();

                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Sets proposal value
 */
export async function setProposal(appConfig: any, proposal: ProposalsItem): Promise<any> {
    console.log('setProposal called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();
                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                
                if (sheetProposal !== null && sheetProposal !== undefined) {
                    const range = sheetProposal.getRange(appConfig.proposal.rangeProposal);
                    range.values = [[proposal.scopeText]];
                    await context.sync();
                    console.log('setProposal done');
                }
                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}

/**
 * Gets contacts list
 */
export async function getContacts(appConfig: any): Promise<any> {
    console.log('getContacts called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();

                const contacts : Contacts = {
                    existsSheet: false,
                    items: []
                };

                const contactsSheet = getWorksheetByName(sheets, appConfig.contact.worksheet);
                
                if (contactsSheet !== null && contactsSheet !== undefined) {
                    contacts.existsSheet = true;
                    console.log('appConfig.contact.rangeContacts: ', appConfig.contact.rangeContacts);

                    //const ranges = contactsSheet.getRange(appConfig.contact.rangeContacts);
                    
                    const tables = contactsSheet.tables;
                    tables.load("name");
                    await context.sync();

                    const table = tables.getItem(appConfig.contact.rangeContacts);
                    if (table !== null && table !== undefined) {
                        var ranges = table.getDataBodyRange();
                        //console.log('ranges: ', ranges);
                        ranges.load('values');
                        await context.sync();
                        //console.log('ranges.values: ', ranges.values);
                        const values = ranges.values;
                        
                        const items = _.map(values, (row) => {
                            //console.log('logosWorksheet = : ', row[1]);
                            return {
                                name: row[0],
                                email: row[2],
                                phone: row[3],
                                mobile: row[4]
                            }
                        });
                        console.log('items: ', items);
                        //console.log('set items');
                        contacts.items = items;
                    }
                }
                resolve(contacts);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Gets proposal list
 */
export async function getProposals(appConfig: any): Promise<any> {
    console.log('getProposals called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                const sheets = context.workbook.worksheets;
                sheets.load('items/name');
                await context.sync();

                const proposals : Proposals = {
                    existsSheet: false,
                    items: []
                };

                const proposalsSheet = getWorksheetByName(sheets, appConfig.proposal.worksheet);
                
                if (proposalsSheet !== null && proposalsSheet !== undefined) {
                    proposals.existsSheet = true;
                    console.log('appConfig.proposal.tableProposals: ', appConfig.proposal.tableProposals);

                    //const ranges = contactsSheet.getRange(appConfig.contact.rangeContacts);
                    
                    const tables = proposalsSheet.tables;
                    tables.load("name");
                    await context.sync();

                    const table = tables.getItem(appConfig.proposal.tableProposals);
                    if (table !== null && table !== undefined) {
                        var ranges = table.getDataBodyRange();
                        //console.log('ranges: ', ranges);
                        ranges.load('values');
                        await context.sync();
                        //console.log('ranges.values: ', ranges.values);
                        const values = ranges.values;
                        
                        const items = _.map(values, (row) => {
                            //console.log('logosWorksheet = : ', row[1]);
                            return {
                                costScope: row[0],
                                rowHeight: row[1],
                                scopeText: row[2]
                            }
                        });
                        console.log('items: ', items);
                        //console.log('set items');
                        proposals.items = items;
                    }
                }
                resolve(proposals);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Gets contacts list
 */
export async function justifyLeft(appConfig: any): Promise<any> {
    console.log('justifyLeft called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {
            Excel.run(async context => {

                // #12 - "Left Justify" all sheets when the task panel opens
                var activeSheet = context.workbook.worksheets.getActiveWorksheet();
                const rangeA1 = activeSheet.getRange('A1');
                console.log('Activate A1');
                rangeA1.select();
                await context.sync();

                resolve(true);
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(false);
            });
        } else {
            console.log('Office.context isnt defined');
            resolve(false);
        }
    });
}


/**
 * Gets the workbook content from Excel application
 */
export async function getWorkbookContent(appConfig: any): Promise<any> {
    console.log('getWorkbookContent called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {

            Excel.run(async context => {
                Office.context.document.getFileAsync(Office.FileType.Pdf, { sliceSize: 100000 },
                result => {
                    console.log(`getWorkbookContent returned the result.status: ${result.status}`);
                    if (result.status == Office.AsyncResultStatus.Succeeded) {
                        console.log('Reads the file by slices');
                        const state = {
                            file: result.value,
                            counter: 0,
                            sliceCount: result.value.sliceCount,
                            documentData: []
                        };

                        const percentCompleted = state.documentData.length / state.file.size;
                        console.log(`stateResult.documentData.length = ${state.file.size}; progressEvent.loaded = 0; percentCompleted = ${percentCompleted}`);

                        getDataSlice(state)
                        .then(stateResult => {
                            console.log(`Received size is ${stateResult.documentData.length}`);
                            context.workbook.application.suspendScreenUpdatingUntilNextSync();
                            closeFile(state)
                            .then(success => {
                                console.log('Reads the file successed');
                                resolve(stateResult.documentData);
                            });
                        });
                        
                        console.log('Finished?!');
                    }
                    else {
                        resolve(null);
                    }
                });
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });            
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

/**
 * Print
 */
export async function print(appConfig: any): Promise<any> {
    console.log('getWorkbookContent called');
    return new Promise((resolve, reject) => {
        if (Office.context !== undefined && Office.context != null) {

            Excel.run(async context => {
                const sheets = context.workbook.worksheets;
                sheets.load('items/name,items/visiblity');
                await context.sync();

                const sheetProposal = getWorksheetByName(sheets, appConfig.logosWorksheetws141);
                if (sheetProposal !== null && sheetProposal !== undefined) 
                    sheetProposal.activate();

                const printSheets = appConfig.printSheets.split(',');
                const items = sheets.items;
                _.forEach(items, item => {
                    const index = _.findIndex(printSheets, print => { return print === item.name});
                    if (index < 0) {
                        item.visibility = Excel.SheetVisibility.hidden;
                    }
                });
                await context.sync();

                Office.context.document.getFileAsync(Office.FileType.Pdf, { sliceSize: 100000 },
                result => {
                    console.log(`getWorkbookContent returned the result.status: ${result.status}`);
                    if (result.status == Office.AsyncResultStatus.Succeeded) {
                        console.log('Reads the file by slices');
                        const state = {
                            file: result.value,
                            counter: 0,
                            sliceCount: result.value.sliceCount,
                            documentData: []
                        };

                        const percentCompleted = state.documentData.length / state.file.size;
                        console.log(`stateResult.documentData.length = ${state.file.size}; progressEvent.loaded = 0; percentCompleted = ${percentCompleted}`);

                        getDataSlice(state)
                        .then(stateResult => {
                            console.log(`Received size is ${stateResult.documentData.length}`);
                            context.workbook.application.suspendScreenUpdatingUntilNextSync();
                            closeFile(state)
                            .then(success => {
                                console.log('Reads the file successed');
                                resolve(stateResult.documentData);
                            });
                        });
                        
                        console.log('Finished?!');
                    }
                    else {
                        resolve(null);
                    }
                });
                _.forEach(items, item => {
                    const index = _.findIndex(printSheets, print => { return print === item.name});
                    if (index < 0) {
                        item.visibility = Excel.SheetVisibility.visible;
                    }
                });
                await context.sync();
            })
            .catch(error => {
                console.log('Error: ', error);
                resolve(null);
            });            
        } else {
            console.log('Office.context isnt defined');
            resolve(null);
        }
    });
}

export const excelApi = {
    activateLastWorksheet,
    activateNextWorksheet,
    activatePreviousWorksheet,
    activateWorksheetByName,
    getActiveWorkseet,
    getWorksheets,
    getCalculationMode,
    setCalculationMode,
    initializeWorkbook,
    getLogos,
    setLogo,
    setContactFormula,
    getContacts,
    setContact,
    setFullContact,
    justifyLeft,
    getWorkbookContent,
    setProposalHeight,
    getProposals,
    setProposal,
    setAutoSizeHeight,
    print
}