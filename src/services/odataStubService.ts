/**
 * Contains implementation of REST STUB API calls
 *
 */

import { isNull, isUndefined } from 'lodash';
import axios from 'axios';
import { store } from '../store';
import { Contacts, ContactsItem } from '../models/contacts.model';

/******************************************************************************
 * Private functions
******************************************************************************/

/**
 * Creates the axios instance

 */
const getAxiosInstance = () => {
    return axios.create({
    });
}

function getFormMultipartHeaders() {
    const headers = {
        'Content-Type': 'multipart/form-data'
    }

    return headers;
}


/******************************************************************************
 * Public functions
******************************************************************************/
export async function getContacts(appConfig: any) {
    console.log(`getContacts called`);

    const axiosInstance = await getAxiosInstance();

    const endpoint = `${appConfig.contactsUrl}`;
    console.log(`getContacts runs method ${endpoint}`);
    try {
        const response = await axiosInstance({
            method: 'GET',
            url: endpoint
        });
        const contacts : Contacts = {
            existsSheet: true,
            items: []
        };
        contacts.items = response.data.contacts;
        return contacts;
    } catch (error) {
        console.log('Error: ', error.message);
        return null;
    }
}

export async function uploadFile(appConfig: any, blob, file: string) {

    console.log(`uploadFile called`);

    const axiosInstance = await getAxiosInstance();

    const endpoint = `${appConfig.uploadFile}`;
    console.log(`uploadFile runs ${endpoint}`);

    const formdata = new FormData();
    formdata.append("file", blob, file);

    try {
        const response = await axiosInstance({
            method: 'POST',
            url: endpoint,
            data: formdata,
            headers: getFormMultipartHeaders()
        });
        //{"taskId":"539"}
        return response.data;
    } catch (error) {
        console.log('Error: ', error.message);
        return null;
    }
}

export const odataStubService = {
    getContacts,
    uploadFile
};

