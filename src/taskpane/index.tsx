import App from "../app";
import { AppContainer } from "react-hot-loader";
import { initializeIcons } from '@uifabric/icons';
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import { store } from '../store/';
import { enableES5} from 'immer'

enableES5();
initializeIcons();

let isOfficeInitialized = false;
let addinLocation = null;

const render = Component => {
	ReactDOM.render(
		<AppContainer>
			<Provider store={store}>
				<Component isOfficeInitialized={isOfficeInitialized} location={addinLocation} />
			</Provider>
		</AppContainer>,
		document.getElementById("container")
	);
};

Office.initialize = () => {
	console.log('window.location.href = ' + window.location.href);
	isOfficeInitialized = true;
	addinLocation = window.location;
	render(App);
};
