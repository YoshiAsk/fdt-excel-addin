import React, { useEffect } from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Provider } from 'react-redux';
import { store } from './store';
import queryString from 'query-string';
import { ScrollablePane, ScrollbarVisibility, Stack } from '@fluentui/react';
import { ExcelMainPage, GotoPage, AboutPage, LogoPage, ContactsPage, ProposalPage,
  GotoDialog, AboutDialog, LogoDialog, ContactDialog, WaitingDialog } from './pages';
import { ErrorBar, Waiting } from './components'
import { appActions } from './store/actions/app/actions';

const App = (props) => {

  const dispatch = useDispatch();

  useEffect(() => {
    console.log('Initialized the application mainpage');
    console.log('props.location=' + props.location);
    dispatch(appActions.initializeAddin(props.location))
  }, [])

  const initializeConfig = (): void => {
    console.log('initializeConfig is called');
  }

  const transformLocation = (inputLocation: Location): Location => {
    
    const queryStringParsed = queryString.parse(inputLocation.search);
    console.log(`queryStringParsed.page == ${queryStringParsed.page}`);
    if (queryStringParsed.page === undefined) {
      console.log(`inputLocation.hash == ${inputLocation.hash}`);
      return {
        ...inputLocation,
        pathname: inputLocation.hash.substring(1),
      };
    }

    return {
      ...inputLocation,
      pathname: "/" + queryStringParsed.page,
    };
  }

  return (
    <div className='wrapper'>
      <ErrorBar />
      <Provider store={store}>
        { initializeConfig() }
        <Router>
          <div className='main'>
            <ScrollablePane scrollbarVisibility={ScrollbarVisibility.auto}> 
              <Stack style={{ paddingLeft: 0, paddingRight: 0}}>
                <Route render={() => (
                  <Switch location={transformLocation(props.location)}>
                    <Route path="/main" component={ExcelMainPage} />
                    <Route path="/goto" component={GotoPage} />
                    <Route path="/about" component={AboutPage} />
                    <Route path="/logo" component={LogoPage} />
                    <Route path="/contacts" component={ContactsPage} />
                    <Route path="/proposal" component={ProposalPage} />
                    <Route path="/dialoggoto" component={GotoDialog} />
                    <Route path="/dialogabout" component={AboutDialog} />
                    <Route path="/dialoglogo" component={LogoDialog} />
                    <Route path="/dialogcontact" component={ContactDialog} />
                    <Route path="/dialogwaiting" component={WaitingDialog} />
                    <Redirect from="*" to="/main" />
                  </Switch>
                )}
                />
              </Stack>
            </ScrollablePane> 
          </div>
        </Router>
        <Waiting />
      </Provider>
    </div>
  )
}

export default App;
