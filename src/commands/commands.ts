import 'core-js'
import { excelApi } from '../services/excelApi';
import axios from 'axios';
import { odataStubService } from '../services/odataStubService';

/* global global, Office, self, window */

Office.onReady(() => {
  console.log('Commands: Office is ready');  
});

const getGlobal = (): any => {
  return typeof self !== "undefined"
    ? self
    : typeof window !== "undefined"
    ? window
    : typeof global !== "undefined"
    ? global
    : undefined;
}

const getConfig = async () => {
  return (await axios.get('assets/config.json')).data;
}

/**
 * Go to worksheet
 * @param event 
 */
const gotoSheet = async (event) => {
  console.log(`gotoSheet function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;

  async function processMessage(arg) {
    //var messageFromDialog = JSON.parse(arg.message);
    dialog.close();
    console.log(`processMessage function is called from button with id ${event.source.id}`);  
    try {
      await excelApi.activateWorksheetByName(arg.message);
     } catch (ex) {
      (console.error || console.log).call(console, ex.stack || ex);
    }
  }

  async function dialogEvent(arg) {
  }

  try {

    const sheets = await excelApi.getWorksheets();
    localStorage.setItem("sheets", JSON.stringify(sheets));
    console.log(`location.host = ${g.location.host}`);
    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialoggoto', {height: 40, width: 20, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {

        dialog = result.value;
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
        dialog.addEventHandler(Office.EventType.DialogEventReceived, dialogEvent);
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

/**
 * Go to latest worksheet
 * @param event 
 */
const gotoLatestSheet = async (event) => {
  console.log(`gotoLatestSheet function is called from button with id ${event.source.id}`);  
  try {
    await excelApi.activateLastWorksheet();
   } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

const gotoNextSheet = async (event) => {
  console.log(`gotoNextSheet function is called from button with id ${event.source.id}`);  
  try {
    await excelApi.activateNextWorksheet();
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}


/**
 * PDF export dialog
 * @param event 
 */
const exportPdf = async (event) => {
  console.log(`exportPdf function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;
  let config = null;

  async function processMessage(arg) {
    console.log(`processMessage function is called from button with id ${event.source.id}`);  
    try {
      var content = await excelApi.getWorkbookContent(config);
      var blob = new Blob([new Uint8Array(content)], {type: 'application/pdf'});
  
      const result = await odataStubService.uploadFile(config, blob, 'export.pdf');
  
      if (result != null)
          window.open(config.uploadFile + '/' + result.file);      
     } catch (ex) {
      (console.error || console.log).call(console, ex.stack || ex);
    }
    dialog.close();
  }

  async function dialogEvent(arg) {
  }

  try {
    config = await getConfig();

    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialogwaiting', {height: 30, width: 20, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {
        dialog = result.value;
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
        dialog.addEventHandler(Office.EventType.DialogEventReceived, dialogEvent);  
        
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

/**
 * Print dialog
 * @param event 
 */
const printPdf = async (event) => {
  console.log(`printPdf function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;
  let config = null;

  async function processMessage(arg) {
    console.log(`processMessage function is called from button with id ${event.source.id}`);  
    try {
      var content = await excelApi.print(config);
      var blob = new Blob([new Uint8Array(content)], {type: 'application/pdf'});
  
      const result = await odataStubService.uploadFile(config, blob, 'print.pdf');
  
      if (result != null)
          window.open(config.uploadFile + '/' + result.file);      
     } catch (ex) {
      (console.error || console.log).call(console, ex.stack || ex);
    }
    dialog.close();
  }

  async function dialogEvent(arg) {
  }

  try {
    config = await getConfig();

    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialogwaiting', {height: 30, width: 20, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {
        dialog = result.value;
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
        dialog.addEventHandler(Office.EventType.DialogEventReceived, dialogEvent);  
        
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

const gotoBackSheet = async (event) => {
  console.log(`gotoBackSheet function is called from button with id ${event.source.id}`);  
  try {
    await excelApi.activatePreviousWorksheet();
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

/**
 * About dialog
 * @param event 
 */
const about = async (event) => {
  console.log(`about function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;

  try {

    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialogabout', {height: 25, width: 20, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {
        dialog = result.value;
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

const setAutoCalculation = async (event) => {
  console.log(`setAutoCalculation function is called from button with id ${event.source.id}`);  
  try {
    await excelApi.setCalculationMode(Excel.CalculationMode.automatic);
    /*
    const tab = {id: "FDTCustomTab", controls: [{id: "FDT.CalcAuto", enabled: false}, {id: "FDT.CalcManual", enabled: true}]};
    let ribbonUpdater = {tabs: [tab]};
    await Office.ribbon.requestUpdate(ribbonUpdater);
    console.log(`setAutoCalculation complete`);*/
  
    // @ts-ignore
    OfficeRuntime.ui.getRibbon()
    // @ts-ignore
    .then(async ribbon => {
      console.log(`requestUpdate called`);
      try {
        console.log(`ribbon ${ribbon}`);
        //if (Office.context.requirements.isSetSupported('RibbonApi', '1.1')) {
          await ribbon.requestUpdate({
            tabs: [
              {
                id: 'FDTCustomTab',
                controls: [
                  {
                    id: 'FDT.CalcAuto',
                    enabled: false
                  },
                  {
                    id: 'FDT.CalcManual',
                    enabled: true
                  }
                ]
              }
            ]
          });
        //}
        //else
        //  console.log(`ribbon API is not supported`);
        console.log(`requestUpdate complete`);
      } catch (ex) {
        (console.error || console.log).call(console, ex.stack || ex);
      }
    });


  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

const setManualCalculation = async (event) => {
  console.log(`setManualCalculation function is called from button with id ${event.source.id}`);  
  try {
    await excelApi.setCalculationMode(Excel.CalculationMode.manual);

    // @ts-ignore
    OfficeRuntime.ui.getRibbon()
    // @ts-ignore
    .then(async ribbon => {
      console.log(`requestUpdate called`);
      try {
        console.log(`ribbon ${ribbon}`);
        //if (Office.context.requirements.isSetSupported('RibbonApi', '1.1')) {
          await ribbon.requestUpdate({
            tabs: [
              {
                id: 'FDTCustomTab',
                controls: [
                  {
                    id: 'FDT.CalcAuto',
                    enabled: true
                  },
                  {
                    id: 'FDT.CalcManual',
                    enabled: false
                  }
                ]
              }
            ]
          });
        //}
        //else
        //  console.log(`ribbon API is not supported`);
        console.log(`requestUpdate complete`);
      } catch (ex) {
        (console.error || console.log).call(console, ex.stack || ex);
      }
    });
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

/**
 * Logo select dialog
 * @param event 
 */
const setLogo = async (event) => {
  console.log(`setLogo  function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;
  let config = null;

  async function processMessage(arg) {
    console.log(`processMessage function is called from button with id ${event.source.id}`);  
    try {
      const logo = JSON.parse(arg.message);
      await excelApi.setLogo(config, logo);
     } catch (ex) {
      (console.error || console.log).call(console, ex.stack || ex);
    }
    dialog.close();
  }

  async function dialogEvent(arg) {
  }

  try {
    config = await getConfig();

    const logos = await excelApi.getLogos(config);
    localStorage.setItem("logos", logos === null || logos === undefined ? null : JSON.stringify(logos));
    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialoglogo', {height: 35, width: 30, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {
        dialog = result.value;
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
        dialog.addEventHandler(Office.EventType.DialogEventReceived, dialogEvent);        
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}

/**
 * Contact select dialog
 * @param event 
 */
const setContact = async (event) => {
  console.log(`setContact function is called from button with id ${event.source.id}`);  

  let dialog: Office.Dialog;
  let config = null;

  async function processMessage(arg) {
    console.log(`processMessage function is called from button with id ${event.source.id}`);  
    try {
      const contact = JSON.parse(arg.message);
      await excelApi.setFullContact(config, contact);
     } catch (ex) {
      (console.error || console.log).call(console, ex.stack || ex);
    }
    dialog.close();
  }

  async function dialogEvent(arg) {
  }

  try {
    config = await getConfig();

    const contacts = await excelApi.getContacts(config);
    localStorage.setItem("contacts", contacts === null || contacts === undefined ? null : JSON.stringify(contacts));
    Office.context.ui.displayDialogAsync('https://' + g.location.host + '/dialogs.html?page=dialogcontact', {height: 40, width: 35, displayInIframe: true},
    (result) => {
      if (result.status === Office.AsyncResultStatus.Failed) {
          console.log('display error');
      }
      else {
        dialog = result.value;
        dialog.addEventHandler(Office.EventType.DialogMessageReceived, processMessage);
        dialog.addEventHandler(Office.EventType.DialogEventReceived, dialogEvent);        
      }
    }
  );
  } catch (ex) {
    (console.error || console.log).call(console, ex.stack || ex);
  }
  event.completed();
}


/**
 * Global scope
 */
const g = getGlobal();

// register functions
g.gotoLatestSheet = gotoLatestSheet;
g.gotoNextSheet = gotoNextSheet;
g.gotoBackSheet = gotoBackSheet;
g.gotoSheet = gotoSheet;
g.about = about;
g.setAutoCalculation = setAutoCalculation;
g.setManualCalculation = setManualCalculation;
g.setLogo = setLogo;
g.setContact = setContact;
g.exportPdf = exportPdf;
g.printPdf = printPdf;
