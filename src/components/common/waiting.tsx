import React from 'react';
import { useSelector } from 'react-redux';
import { Overlay, Spinner, SpinnerSize, Stack, ProgressIndicator, getTheme, mergeStyleSets } from '@fluentui/react';
import './styles/waiting.css';

const theme = getTheme();

const Waiting = () => {

    const appState = useSelector(state => state.appReducer);
    const fileLoadingOperation = useSelector(state => state.appReducer.longOperation);
   
    const styles = mergeStyleSets({
        root: {
            background: theme.palette.white,
            padding: 20,
            margin: 20
        }
    })

    const getSpinner = () => (
        <Stack tokens={{ childrenGap: 15 }} horizontalAlign='center' className={styles.root}>
            <Spinner size={SpinnerSize.large} />
            <div className="ms-fontWeight-bold ms-fontSize-12 waiting-text">
                { appState.translate(fileLoadingOperation.fetchMessage) }
            </div>
        </Stack>
    );

    const DarkOverlay = () => (
        <Overlay isDarkThemed={true} style={{ zIndex: 1 }}>
            <div className="waiting-panel">
                { getSpinner() }
            </div>
        </Overlay>
    );

    return (
        <div>
        { fileLoadingOperation.isFetched ?  <DarkOverlay /> : <div/>
        }
        </div>
    )
}

export default Waiting;
