import React, { useEffect }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { excelApi } from '../../services/excelApi'
import { Stack, Text, FontIcon, Image } from '@fluentui/react';

const About = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  const getName = () => {
    if (appState.appConfig.appName.length <= 0)
      return '';
    else
      return `${appState.appConfig.appName}`;
  }

  const getVersion = () => {
    if (appState.appConfig.appVersion.length <= 0)
      return '';
    else
      return `${appState.translate('about_version')} ${appState.appConfig.appVersion}`;
  }

  const getDate = () => {
    if (appState.appConfig.appDate.length <= 0)
      return '';
    else
      return `${appState.translate('about_date')} ${appState.appConfig.appDate}`;
  }

  return ( 
    <Stack>
      <Stack horizontal horizontalAlign="center" verticalAlign="start" style={{ marginTop: 0 }} tokens={{ childrenGap: 10 }}>
        <FontIcon iconName='InfoSolid' style={{ fontSize: 32, marginTop: 0, color: '#0080C0'}} />
        <Stack tokens={{ childrenGap: 5 }}>
          <Text style={{ fontSize:20, color:'#5e88b8' }}>{ getName() }</Text>
          <Text style={{ fontSize:14, marginTop:20 }}>{ getVersion() }</Text>
          <Text style={{ fontSize:14 }}>{ getDate() }</Text>
        </Stack>
      </Stack>
    </Stack>
  )
}

export default About;
