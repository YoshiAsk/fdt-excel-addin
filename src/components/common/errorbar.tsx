import * as React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Stack, MessageBar, MessageBarType } from '@fluentui/react';

const ErrorBar = () => {

  const error = useSelector(state => state.appReducer.error);
  const dispatch = useDispatch();

  const resetError = () => {
    dispatch({ type:'app.APP_CLEAR_ERRORS' });
  };

  
  const getErrorBar = () => (
    <Stack style={{ marginLeft:0, marginRight:0, position:'absolute', bottom: 0, right: 0, left: 0, zIndex:1000 }}>
      <MessageBar messageBarType={MessageBarType.error} isMultiline={true} onDismiss={resetError} dismissButtonAriaLabel="Close">
        {error}
      </MessageBar>
    </Stack>
  );

  return ( 
    error !== null && error !== undefined ?
      getErrorBar() : <div/>
  )
}

export default ErrorBar;
