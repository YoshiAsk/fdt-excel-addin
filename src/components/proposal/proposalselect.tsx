import React, { useEffect, useState }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types'
import { Stack, Text, SearchBox, ITheme, getTheme, getFocusStyle, 
  DetailsList, SelectionMode, DetailsListLayoutMode, mergeStyleSets, FontIcon } from '@fluentui/react';

const ProposalSelect = (props) => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  const [filter, setFilter] = useState('');

  const theme: ITheme = getTheme();
  const { palette, semanticColors, fonts } = theme;
  const classNames = mergeStyleSets({
    itemCell: [
      getFocusStyle(theme, { inset: -1 }),
      {
        selectors: {
          '&:hover': { background: palette.neutralLight },
        },
      },
    ]
  })


  const onChange = async (proposal) => {
    console.log(`onChange click`);
    if (props.onChangeProposal !== null && props.onChangeProposal !== undefined) {
      props.onChangeProposal(proposal);
    }
  }

  const renderProposals = () => {
    let items = props.proposals.items;
    console.log(`filter: `, filter);
    if (filter !== '')
      items = _.filter(items, item => item.costScope.toLowerCase().indexOf(filter.toLowerCase()) >= 0);
    return <Stack>
      {
        props.proposals.existsSheet === true ?

        <Stack>
          <Stack horizontalAlign="start" style={{ display: 'inline-block', width: '85vw'}}>
            <SearchBox placeholder="Proposal search" value={filter} 
              onClear={ev => {setFilter('');}}
              onSearch={newValue => {setFilter(newValue); console.log('filter: ', filter);}} disableAnimation />
          </Stack>
          <Stack style={{ width:'100%', display: 'inline-block', marginTop:10, marginBottom:0 }} tokens={{ childrenGap: 10 }}>

            <Stack horizontal horizontalAlign='start' verticalAlign='center' tokens={{ childrenGap: 10 }}>
              <Stack horizontalAlign='stretch' verticalAlign='center' wrap style={{ }}>
                <Text style={{ textAlign: 'start', fontSize:13, fontWeight: 'bold', paddingRight: 3, paddingTop: 3, paddingBottom: 3 }}>{ appState.translate('proposal_costscope') }</Text>          
              </Stack>
            </Stack>
            {
              _.map(items, (item, index) => {
                return (
                  <Stack horizontal horizontalAlign='start' className={classNames.itemCell} verticalAlign='center' tokens={{ childrenGap: 10 }}
                    style={{ cursor: 'pointer' }} 
                    onClick={() => {onChange(item)}}>
                    <Stack horizontalAlign='stretch' verticalAlign='center' wrap style={{ }}>
                      <Text style={{ textAlign: 'start', fontSize:13, paddingRight: 3, paddingTop: 3, paddingBottom: 3 }}>{ item.costScope }</Text>          
                    </Stack>                    
                  </Stack>
                );
              })
            }

          </Stack>
        </Stack>        
        :
        <Text style={{ fontSize:14 }}>{ appState.translate('proposal_notexists')}</Text>
      }
    </Stack>; 
  }

  return ( 
    <Stack horizontalAlign="stretch" verticalAlign="start" style={{ marginTop: 0 }} tokens={{ childrenGap: 10 }}>
      <Stack style={{ marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
        <Text style={{ fontSize:16, color:'#5e88b8' }}>{ appState.translate('proposal_title') }</Text>
      </Stack>
      { props.proposals !== null && props.proposals !== undefined ? renderProposals() : <></> }
    </Stack>
  )
}

export default ProposalSelect;

ProposalSelect.propTypes = {
  proposals: PropTypes.any,
  onChangeProposal: PropTypes.func
}