import React, { useEffect }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types'
import { Stack, Text, Image, IColumn, Selection, ITheme, getTheme, getFocusStyle, 
  DetailsList, SelectionMode, DetailsListLayoutMode, mergeStyleSets, FontIcon } from '@fluentui/react';

const SheetSelect = (props) => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  const theme: ITheme = getTheme();
  const { palette, semanticColors, fonts } = theme;
  const classNames = mergeStyleSets({
    itemCell: [
      getFocusStyle(theme, { inset: -1 }),
      {
        selectors: {
          '&:hover': { background: palette.neutralLight },
        },
      },
    ]
  })


  const onOpen = async (sheet) => {
    console.log(`onChange click`);
    if (props.onChangeSheet !== null && props.onChangeSheet !== undefined) {
      props.onChangeSheet(sheet);
    }
  }

  const renderSheets = () => {
    return <Stack style={{ width:'100%'}}>
      {
        _.map(props.sheets, (item, index) => {
          return (
            <Stack style={{ width:'100%', display: 'inline-block', marginTop:10, marginBottom:0 }} tokens={{ childrenGap: 10 }}>

              <Stack horizontal tokens={{ childrenGap: 5 }} className={classNames.itemCell} horizontalAlign="start" 
                style={{ cursor: 'pointer' }}
                title={ item } 
                onClick={() => {onOpen(item)}}>
                <Stack style={{ width: 25, minWidth: 25 }}>
                  <Image style={{paddingLeft: 3, paddingTop:3, paddingBottom:3 }} src="/assets/worksheet-image-20.png" /> 
                </Stack>
                <Stack  horizontalAlign='stretch' verticalAlign='center' wrap>
                  <Text style={{ textAlign: 'start', fontSize:13, paddingRight: 3, paddingTop: 3, paddingBottom: 3, display:'inline-block', textOverflow: 'ellipsis' }}>{ item }</Text>          
                </Stack>
              </Stack>
            </Stack>
          );
        })

      }
    </Stack>; 
  }

  return ( 
    <Stack horizontalAlign="start" verticalAlign="start" style={{ marginTop: 0 }} tokens={{ childrenGap: 10 }}>
        <Stack wrap style={{ marginTop:20, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <Text style={{ fontSize:16, color:'#5e88b8' }}>{ appState.translate('goto_title') }</Text>
        </Stack>
        { renderSheets() }
    </Stack>
  )

}

export default SheetSelect;

SheetSelect.propTypes = {
  sheets: PropTypes.any,
  onChangeSheet: PropTypes.func
}