import React, { useEffect }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types'
import { Stack, Text, Image, IColumn, Selection, ITheme, getTheme, getFocusStyle, 
  DetailsList, SelectionMode, DetailsListLayoutMode, mergeStyleSets, FontIcon } from '@fluentui/react';

const LogoSelect = (props) => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  const theme: ITheme = getTheme();
  const { palette, semanticColors, fonts } = theme;
  const classNames = mergeStyleSets({
    itemCell: [
      getFocusStyle(theme, { inset: -1 }),
      {
        selectors: {
          '&:hover': { background: palette.neutralLight },
        },
      },
    ]
  })


  const onChange = async (logo) => {
    console.log(`onChange click`);
    if (props.onChangeLogo !== null && props.onChangeLogo !== undefined) {
      props.onChangeLogo(logo);
    }
  }

  const renderLogos = () => {
    return <Stack>
      {
        props.logos.existsSheet === true ?
        <Stack style={{ width:'100%', display: 'inline-block', marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 10 }}>
          <Stack horizontal horizontalAlign='start' verticalAlign='center' tokens={{ childrenGap: 10 }}>
            <Stack style={{ width: 80, minWidth: 80 }}>
              <Text style={{ textAlign: 'start', fontSize:13, fontWeight: 'bold', paddingLeft: 3, paddingTop: 3, paddingBottom: 3 }}>{ appState.translate('logo_name') }</Text> 
            </Stack>
            <Stack style={{ width: 55, minWidth: 55 }}>
              <Text style={{ textAlign: 'start', fontSize:13, fontWeight: 'bold', paddingTop: 3, paddingBottom: 3 }}>{ appState.translate('logo_acronym') }</Text> 
            </Stack>
            <Stack horizontalAlign='stretch' verticalAlign='center' wrap style={{ }}>
              <Text style={{ textAlign: 'start', fontSize:13, fontWeight: 'bold', paddingRight: 3, paddingTop: 3, paddingBottom: 3 }}>{ appState.translate('logo_company') }</Text>          
            </Stack>
          </Stack>
          {
            _.map(props.logos.items, (item, index) => {
              return (
                <Stack horizontal horizontalAlign='start' className={classNames.itemCell} verticalAlign='center' tokens={{ childrenGap: 10 }}
                  style={{ cursor: 'pointer' }} 
                  onClick={() => {onChange(item)}}>
                  <Stack style={{ width: 80, minWidth: 80 }}>
                    <Text style={{ textAlign: 'start', fontSize:13, paddingLeft: 3, paddingTop: 3, paddingBottom: 3 }}>{ item.logoName }</Text> 
                  </Stack>
                  <Stack style={{ width: 55, minWidth: 55 }}>
                    <Text style={{ textAlign: 'start', fontSize:13, paddingTop: 3, paddingBottom: 3 }}>{ item.acronym }</Text> 
                  </Stack>
                  <Stack horizontalAlign='stretch' verticalAlign='center' wrap style={{ }}>
                    <Text style={{ textAlign: 'start', fontSize:13, paddingRight: 3, paddingTop: 3, paddingBottom: 3 }}>{ item.company }</Text>          
                  </Stack>
                </Stack>
              );
            })
          }

        </Stack>
        :
        <Text style={{ fontSize:14 }}>{ appState.translate('logo_notexists')}</Text>
      }
    </Stack>; 
  }

  return ( 
    <Stack horizontalAlign="stretch" verticalAlign="start" style={{ marginTop: 0 }} tokens={{ childrenGap: 10 }}>
      <Stack style={{ marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
        <Text style={{ fontSize:16, color:'#5e88b8' }}>{ appState.translate('logo_title') }</Text>
      </Stack>
      { props.logos !== null && props.logos !== undefined ? renderLogos() : <></> }
    </Stack>
  )
}

export default LogoSelect;

LogoSelect.propTypes = {
  logos: PropTypes.any,
  onChangeLogo: PropTypes.func
}