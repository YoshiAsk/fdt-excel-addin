import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import _ from 'lodash';
import { Stack, Image, CommandBarButton, IImageProps, Text, IButtonProps, MessageBar } from '@fluentui/react';
import { excelOfficeActions } from '../store/actions/office/excel/actions';
import { apiActions } from '../store/actions/api/actions';
import { excelApi } from '../services/excelApi'

const MainPage = () => {

  const appState = useSelector(state => state.appReducer);
  const excelState = useSelector(state => state.excelOfficeReducer);
  const dispatch = useDispatch();

  const [currentSheet, setCurrentSheet ] = React.useState('');
  const [sheets, setSheets ] = React.useState([]);
  const [calculationMode, setCalculationMode ] = React.useState('');

  useEffect(() => {
    async function getCalculationMode() {
      console.log('getCalculationMode called');
      const calculationMode = await excelApi.getCalculationMode();
      setCalculationMode(calculationMode);
      //dispatch(apiActions.getContacts());
    }

    getCalculationMode();
  }, [calculationMode]);

  const imageProps: Partial<IImageProps> = {
    width: 32,
    height: 32,
  };

  const buttonProps1 = {
    width: 70,
    height: 75,
    verticalAlign: 'top',
  };

  const buttonProps2 = {
    width: 70,
    height: 95,
    verticalAlign: 'top',
  };

  const buttonProps = {
    width: 70,
    height: 95,
    verticalAlign: 'top',
  };

  const onBack = async () => {
    console.log('onBack method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'open_worksheet'} }); 
    dispatch(excelOfficeActions.openBackWorksheet()); 
  }

  const onNext = async () => {
    console.log('onNext method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'open_worksheet'} });
    dispatch(excelOfficeActions.openNextWorksheet()); 
  }

  const onLast = async () => {
    console.log('onLast method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'open_worksheet'} });
    dispatch(excelOfficeActions.openLastWorksheet()); 
  }

  const onGoto = async () => {
    console.log('onGoto method called');
    history.replace('/goto'); 
  }

  const onAbout = async () => {
    console.log('onAbout method called');
    history.replace('/about'); 
  }

  const getAutoDisabled = () => {
    return calculationMode === Excel.CalculationMode.automatic ? true: false;
  }

  const getManualDisabled = () => {
    return calculationMode === Excel.CalculationMode.manual ? true: false;
  }

  const onAutoCalc = async () => {
    console.log('onNext method called');
    dispatch(excelOfficeActions.setCalculationMode(Excel.CalculationMode.automatic)); 
    setCalculationMode(Excel.CalculationMode.automatic);
  }

  const onManualCalc = async () => {
    console.log('onNext method called');
    dispatch(excelOfficeActions.setCalculationMode(Excel.CalculationMode.manual)); 
    setCalculationMode(Excel.CalculationMode.manual);
  }

  const onLogoSelect = async () => {
    console.log('onLogoSelect method called');
    history.replace('/logo'); 
  }

  const onContacts = async () => {
    console.log('onContacts method called');
    history.replace('/contacts'); 
  }

  const onProposal = async () => {
    console.log('onProposal method called');
    history.replace('/proposal'); 
  }

  const getShapeNotification = () => {
    return (appState.currentShape !== null && appState.currentShape !== undefined) ?
    <MessageBar>
      Activated shape: type={appState.currentShape.type} name={appState.currentShape.name}
    </MessageBar>
    : <></>

  }

  const onExportPdf = async () => {
    console.log('onExportPdf method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'convert_pdf'} });
    dispatch(excelOfficeActions.convertToPdf()); 
  }

  const onPrint = async () => {
    console.log('onPrint method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'print_pdf'} });
    dispatch(excelOfficeActions.print()); 
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 10 }} style={{ paddingTop:20, width:'100%' }}>
        { getShapeNotification() }
        <Stack horizontal style={{marginTop: 20}} tokens={{ childrenGap: 20 }} horizontalAlign="center" verticalAlign="start">
          <CommandBarButton className="button" style={buttonProps1} title={appState.translate('button_goto_tooltip')} onClick={onGoto}>
            <Stack verticalAlign="start" horizontalAlign="center">
              <Image {...imageProps} src="assets/goto-32.png" />
              <Text className="button_text" nowrap>{ appState.translate('button_goto') }</Text>
            </Stack>
          </CommandBarButton>  
          <CommandBarButton className="button" style={buttonProps2} title={appState.translate('button_last_tooltip')} onClick={onLast}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/last-sheet-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_last') }</Text>
            </Stack>
          </CommandBarButton>
          <CommandBarButton className="button" style={buttonProps2} title={appState.translate('button_devmode_tooltip')}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/devmode-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_devmode') }</Text>
            </Stack>
          </CommandBarButton>          
        </Stack>
        <Stack horizontal style={{marginTop: 20}} tokens={{ childrenGap: 20 }} horizontalAlign="center" verticalAlign="start">
          <CommandBarButton className="button" style={buttonProps2} title={appState.translate('button_refresh_tooltip')}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/refresh-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_refresh') }</Text>
            </Stack>
          </CommandBarButton> 
          <CommandBarButton className="button" style={buttonProps} 
            disabled={ getAutoDisabled() } onClick={ onAutoCalc }
            title={appState.translate('button_calcauto_tooltip')}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/calc-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_calcauto') }</Text>
            </Stack>
          </CommandBarButton> 
          <CommandBarButton className="button" style={buttonProps} 
            disabled={ getManualDisabled() } onClick={ onManualCalc }
            title={appState.translate('button_calcmanual_tooltip')}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/calc-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_calcmanual') }</Text>
            </Stack>
          </CommandBarButton>           
        </Stack> 

        <Stack horizontal style={{marginTop: 20}} tokens={{ childrenGap: 20 }} horizontalAlign="center" verticalAlign="start">
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_exportpdf_tooltip')} onClick={ onExportPdf }>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/export2pdf-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_exportpdf') }</Text>
            </Stack>
          </CommandBarButton>
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_print_tooltip')} onClick={ onPrint }>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/print-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_print') }</Text>
            </Stack>
          </CommandBarButton>          
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_logoselect_tooltip')} onClick={ onLogoSelect }>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/logo-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_logoselect') }</Text>
            </Stack>
          </CommandBarButton>
        </Stack>
        <Stack horizontal style={{marginTop: 20}} tokens={{ childrenGap: 20 }} horizontalAlign="center" verticalAlign="start">
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_contacts_tooltip')} onClick={ onContacts }>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/contacts-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_contacts') }</Text>
            </Stack>
          </CommandBarButton>   
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_proposal_tooltip')} onClick={ onProposal }>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/proposal-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_proposal') }</Text>
            </Stack>
          </CommandBarButton>  
          <CommandBarButton className="button" style={buttonProps} title={appState.translate('button_about_tooltip')} onClick={onAbout}>
            <Stack verticalAlign="start" horizontalAlign="center" style={{ width:'100%', display: 'inline-block', textAlign:'center'}}>
              <Stack horizontalAlign="center">
                <Image {...imageProps} src="assets/about-32.png" />
              </Stack>
              <Text style={{ whiteSpace:'normal', display: 'inline-block', textAlign:'center' }} className="button_text">{ appState.translate('button_about') }</Text>
            </Stack>
          </CommandBarButton>                   
        </Stack>        
      </Stack>
    </div>  
  )
}

export default MainPage;
