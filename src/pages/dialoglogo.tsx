import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { LogoSelect } from '../components'
import { Stack, FontIcon} from '@fluentui/react';
import { excelApi } from '../services/excelApi'

const LogoDialog = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();
  const [logos, setLogos] = useState(null);

  useEffect(() => {
    async function loadLogos() {
      console.log('loadLogos called');
      const itemLogos = localStorage.getItem("logos");
      if (itemLogos === null || itemLogos === undefined) {
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getlogos') } });
        return;
      }
      const logos = JSON.parse(itemLogos);
      if (logos === null || logos === undefined)
        dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getlogos') } });
      else
        setLogos(logos);
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'logo_loading'} });
    loadLogos();
  }, []);

  const onChange = async (logo) => {
    console.log(`onChange logo click`);
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'logo_setlogo'} });
    Office.context.ui.messageParent(JSON.stringify(logo));
    return;
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 0, width: '100%'}}>
        <Stack wrap style={{ marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <LogoSelect logos={logos} onChangeLogo={onChange} />
        </Stack>
      </Stack>
    </div>  
  )
}

export default LogoDialog;
