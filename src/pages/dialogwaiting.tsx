import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import _ from 'lodash';
import { Stack, FontIcon, Text, mergeStyleSets, getFocusStyle, getTheme, ITheme, List, Image} from '@fluentui/react';
import { excelApi } from '../services/excelApi'
import { excelOfficeActions } from '../store/actions/office/excel/actions';

const WaitingDialog = () => {

  const appState = useSelector(state => state.appReducer);
  const excelState = useSelector(state => state.excelOfficeReducer);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'convert_pdf'} });
    Office.context.ui.messageParent("");
  }, []);



  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 10, marginBottom: 10, width: '100%'}}>

      </Stack>
    </div>  
  )
}

export default WaitingDialog;
