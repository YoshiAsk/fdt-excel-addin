import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import { About } from '../components'
import { Stack, FontIcon} from '@fluentui/react';


const AboutDialog = () => {

  const appState = useSelector(state => state.appReducer);

  const onBackMain = async (e) => {
    console.log('onBackMain method called');
    history.replace('/main');
  };


  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 20, width: '100%'}}>
        <Stack wrap style={{ marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <About />
        </Stack>
      </Stack>
    </div>  
  )
}

export default AboutDialog;
