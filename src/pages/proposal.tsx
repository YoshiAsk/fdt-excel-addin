import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import { ProposalSelect } from '../components'
import { excelApi } from '../services/excelApi'
import { Stack, FontIcon} from '@fluentui/react';
import { excelOfficeActions } from '../store/actions/office/excel/actions';
import { apiActions } from '../store/actions/api/actions';
import { odataStubService } from '../services/odataStubService';
import { Proposals, ProposalsItem } from '../models/proposals.model';

const ProposalPage = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();
  
  const [proposals, setContacts] = useState(null);

  const onBackMain = async (e) => {
    console.log('onBackMain method called');
    history.replace('/main');
  };

  const onChange = async (proposal) => {
    console.log('onChange proposal method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'proposal_setproposal'} });
    dispatch(excelOfficeActions.setProposal(proposal)); 
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 20, width: '100%'}}>
        <Stack horizontal tokens={{ childrenGap: 10 }} horizontalAlign='start'>
          <FontIcon iconName="Back" title={ appState.translate('proposal_back') } style={{ cursor: 'pointer', fontSize: 20 }} onClick={onBackMain}  />
        </Stack>
        <Stack wrap style={{ marginTop:20, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <ProposalSelect proposals={appState.proposals} onChangeProposal={onChange} />
        </Stack>
      </Stack>
    </div>  
  )
}

export default ProposalPage;
