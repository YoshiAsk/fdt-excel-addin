import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import _ from 'lodash';
import { Stack, FontIcon, Text, mergeStyleSets, getFocusStyle, getTheme, ITheme, List, Image} from '@fluentui/react';
import { excelApi } from '../services/excelApi'
import { excelOfficeActions } from '../store/actions/office/excel/actions';
import { SheetSelect } from '../components'

const GotoPage = () => {

  const appState = useSelector(state => state.appReducer);
  const excelState = useSelector(state => state.excelOfficeReducer);
  const dispatch = useDispatch();
  
  const [currentSheet, setCurrentSheet ] = React.useState('');
  const [state, setState] = useState({
    sheets: []
  });

  useEffect(() => {
    async function loadWorksheets() {
      console.log('loadWorksheets called');
      const  sheets = await excelApi.getWorksheets();
      const newsheets = [];
      sheets.sort();
      newsheets.push(sheets);
      state.sheets = newsheets;
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'goto_loading'} });
    loadWorksheets();
  }, [state]);

  const onBackMain = async (e) => {
    console.log('onBackMain method called');
    history.replace('/main');
  };

  const onOpen = async (sheet) => {
    console.log('onOpen method called');
    setCurrentSheet(sheet);
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'open_worksheet'} });
    dispatch(excelOfficeActions.openWorksheet(sheet)); 
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 20, width: '100%'}}>
        <Stack horizontal tokens={{ childrenGap: 10 }} horizontalAlign='start'>
          <FontIcon iconName="Back" title={ appState.translate('goto_back') } style={{ cursor: 'pointer', fontSize: 20 }} onClick={onBackMain}  />
        </Stack>
        <SheetSelect sheets={state.sheets} onChangeSheet={onOpen} />
      </Stack>
    </div>  
  )
}

export default GotoPage;
