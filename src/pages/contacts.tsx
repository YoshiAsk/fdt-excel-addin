import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import { ContactSelect } from '../components'
import { excelApi } from '../services/excelApi'
import { Stack, FontIcon} from '@fluentui/react';
import { excelOfficeActions } from '../store/actions/office/excel/actions';
import { apiActions } from '../store/actions/api/actions';
import { odataStubService } from '../services/odataStubService';
import { Contacts, ContactsItem } from '../models/contacts.model';

const ContactsPage = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();
  
  const [contacts, setContacts] = useState(null);

  useEffect(() => {
    async function loadContacts() {
      console.log('loadContacts called');
      const contacts: Contacts = await odataStubService.getContacts(appState.appConfig);
      if (contacts !== null && contacts !== undefined) {
          dispatch({ type:'app.APP_INITIALIZE_CONTACTS', payload: { contacts: contacts } });
      } else {
          dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getcontacts') } });
      }
      setContacts(contacts);

      /*const contacts = appState.contacts;
      //console.log(`The contacts table '${contacts}'`);
      setContacts(contacts);*/
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'contacts_loading'} });
    loadContacts();
  }, []);

  const onBackMain = async (e) => {
    console.log('onBackMain method called');
    history.replace('/main');
  };

  const onChange = async (contact) => {
    console.log('onChange contact method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'contacts_setcontact'} });
    dispatch(excelOfficeActions.setContact(contact)); 
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 20, width: '100%'}}>
        <Stack horizontal tokens={{ childrenGap: 10 }} horizontalAlign='start'>
          <FontIcon iconName="Back" title={ appState.translate('contacts_back') } style={{ cursor: 'pointer', fontSize: 20 }} onClick={onBackMain}  />
        </Stack>
        <Stack wrap style={{ marginTop:20, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <ContactSelect contacts={contacts} onChangeContact={onChange} />
        </Stack>
      </Stack>
    </div>  
  )
}

export default ContactsPage;
