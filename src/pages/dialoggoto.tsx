import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import _ from 'lodash';
import { Stack, FontIcon, Text } from '@fluentui/react';
import { excelApi } from '../services/excelApi'
import { excelOfficeActions } from '../store/actions/office/excel/actions';
import { SheetSelect } from '../components'

const GotoDialog = () => {

  const appState = useSelector(state => state.appReducer);
  const excelState = useSelector(state => state.excelOfficeReducer);
  const dispatch = useDispatch();
  
  const [currentSheet, setCurrentSheet ] = React.useState('');
  const [state, setState] = useState({
    sheets: []
  });

  useEffect(() => {
    async function loadWorksheets() {
      console.log('loadWorksheets called');
      const sheets = JSON.parse(localStorage.getItem("sheets"));
      const newsheets = [];
      _.forEach(_.orderBy(sheets, item => item.toLowerCase()), sheet => {
        newsheets.push(sheet);
      });
      state.sheets = newsheets;
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'goto_loading'} });
    loadWorksheets();
  }, [state]);

  const onOpen = async (sheet) => {
    Office.context.ui.messageParent(sheet);
    return;
  }


  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 0, width: '100%'}}>
        <SheetSelect sheets={state.sheets} onChangeSheet={onOpen} />
      </Stack>
    </div>  
  )
}

export default GotoDialog;
