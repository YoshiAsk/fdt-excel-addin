import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { history } from '../helpers';
import { LogoSelect } from '../components'
import { excelApi } from '../services/excelApi'
import { Stack, FontIcon} from '@fluentui/react';
import { excelOfficeActions } from '../store/actions/office/excel/actions';

const LogoPage = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();
  
  const [logos, setLogos] = useState(null);

  useEffect(() => {
    async function loadLogos() {
      console.log('loadLogos called');
      const logos = await excelApi.getLogos(appState.appConfig);
      console.log(`The logos table '${logos}'`);
      if (logos === null || logos === undefined)
        dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getlogos') } });
      else
        setLogos(logos);
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'logo_loading'} });
    loadLogos();
  }, []);

  const onBackMain = async (e) => {
    console.log('onBackMain method called');
    history.replace('/main');
  };

  const onChange = async (logo) => {
    console.log('onChamge logo method called');
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'logo_setlogo'} });
    dispatch(excelOfficeActions.setLogo(logo)); 
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 20, width: '100%'}}>
        <Stack horizontal tokens={{ childrenGap: 10 }} horizontalAlign='start'>
          <FontIcon iconName="Back" title={ appState.translate('logo_back') } style={{ cursor: 'pointer', fontSize: 20 }} onClick={onBackMain}  />
        </Stack>
        <Stack wrap style={{ marginTop:20, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <LogoSelect logos={logos} onChangeLogo={onChange} />
        </Stack>
      </Stack>
    </div>  
  )
}

export default LogoPage;
