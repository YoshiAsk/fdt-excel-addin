import React, { useState, useEffect, ChangeEvent }  from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ContactSelect } from '../components'
import { Stack, FontIcon} from '@fluentui/react';
import { excelApi } from '../services/excelApi'

const ContactDialog = () => {

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();
  const [contacts, setContacts] = useState(null);

  useEffect(() => {
    async function loadContacts() {
      console.log('loadContacts called');
      const itemContacts = localStorage.getItem("contacts");
      if (itemContacts === null || itemContacts === undefined) {
        dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
        dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getcontacts') } });
        return;
      }
      const contacts = JSON.parse(itemContacts);
      if (contacts === null || contacts === undefined)
        dispatch({ type:'app.APP_DISPLAY_ERROR', payload: { error: appState.translate('error_getcontacts') } });
      else
        setContacts(contacts);
      dispatch({ type:'app.APP_STOP_FETCH', payload: { } });
    }

    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'contacts_loading'} });
    loadContacts();
  }, []);

  const onChange = async (contact) => {
    console.log(`onChange contact click`);
    dispatch({ type:'app.APP_START_FETCH', payload: { fetchMessage: 'contacts_setcontact'} });
    Office.context.ui.messageParent(JSON.stringify(contact));
    return;
  }

  return (
    <div className="ms-welcome__main">
      <Stack tokens={{ childrenGap: 20 }} style={{ marginTop: 0, width: '100%'}}>
        <Stack wrap style={{ marginTop:0, marginBottom:0 }} tokens={{ childrenGap: 5 }}>
          <ContactSelect contacts={contacts} onChangeContact={onChange} />
        </Stack>
      </Stack>
    </div>  
  )
}

export default ContactDialog;
