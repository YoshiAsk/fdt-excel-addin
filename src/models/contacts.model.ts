export type ContactsItem = {
  name: string,
  email: string,
  phone: string,
  mobile: string
}

export type Contacts = {
  existsSheet: boolean,
  items: ContactsItem[]
}
