export type LogosItem = {
  index: number,
  logoName: string,
  acronym: string,
  company: string
}

export type Logos = {
  existsSheet: boolean,
  items: LogosItem[]
}
