export type ProposalsItem = {
  costScope: string,
  rowHeight: number,
  scopeText: string
}

export type Proposals = {
  existsSheet: boolean,
  items: ProposalsItem[]
}
