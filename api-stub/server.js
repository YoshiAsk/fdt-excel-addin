var express       = require('express');
var https         = require( 'https' );
var fs            = require( 'fs' );
var request       = require('request')
var cors          = require('cors');
var multer        = require('multer')


var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads')
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + '.pdf')
  }
})

var upload = multer({ storage: storage });

const app           = express();
const port = process.env.PORT || 4300;

/*const httpsOptions = {
  ca: fs.readFileSync('./ssl/ca.crt', 'utf8'), //certificate concatenation or intermediate certificates
  key: fs.readFileSync('./ssl/localhost.key', 'utf8'), //SSL key
  cert: fs.readFileSync('./ssl/localhost.crt', 'utf8') //the certificate
};*/

app.use(cors());

function getFile(file) {
  // read binary data
  var data = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return data;
}

function clearOldFiles(path) {
  try {
    var files = fs.readdirSync(path);
    for (var i = 0; i < files.length; i++) {
      console.log(`file ${files[i]}`);
      var filePath = path + '/' + files[i];
      if (files[i] !== '.marker') {
        fs.unlinkSync(filePath);
      }
    }
  }
  catch(e) { 
    console.log(`error ${e}`);
  }
}

clearOldFiles('./uploads');

app.use(cors());

app.get('/api/v1.0/contacts', (request, response, next) => {
	console.log('api/v1.0/contacts called');
	let versionsData = require('./contacts.json');
	response.json(versionsData);
});

app.post('/api/v1.0/files', upload.single('file'), function (request, response, next) {

  console.log('execute /api/v1.0/files');
  
	response.json({ file: request.file.filename });
});

app.get('/api/v1.0/files/:file', (request, response, next) => {
  console.log('request.params.file == ' + request.params.file);
  let file = request.params.file;
  let data = getFile(`./uploads/${file}`);

  response.writeHead(200, {
    'Content-Type': 'application/pdf',
    'Content-Disposition': `filename="export.pdf"`,
    'Content-Length': Buffer.byteLength(data)
  });

  response.end(data);
});


/*https.createServer(httpsOptions, app)
.listen(port, () =>
    console.log(`\Example app listening on port ${port}!`),
);
*/
app.listen(port, () => {
	console.log("Server running on port :" + port);
});
